const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./src/**/*.{html,js}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: "transparent",
      black: colors.black,
      white: colors.white,
      gray: colors.coolGray,
      primary: colors.emerald,
      language: {
        none: colors.red[500],
        basic: colors.orange[500],
        advanced: colors.yellow[500],
        fluent: colors.lime[500],
        native: colors.green[500],
      },
    },
    extend: {
      width: {
        lg: "1024px",
        "2xl": "1280px",
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
      },
      animation: {
        "spin-slow": "spin 3s linear infinite",
      },
    },
  },
  variants: {
    display: ["responsive", "group-hover", "group-focus"],
  },
  plugins: [],
};
