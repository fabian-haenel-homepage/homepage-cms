# CMS Backend for my homepage

## Running

Load environment variables

```shell
export $(cat example.env | xargs)
```

## Code formatting

### python

For python [black](https://github.com/psf/black) is used.

Execute black

```shell
black .
```

To start black in daemon mode (e.g. to use with IDEs)

```shell
blackd --bind-port 9090
```

### json | yaml | html | md

For various none-python files [prettier](https://prettier.io/) is used.

Execute prettier

```shell
npx prettier -w .
```

### Script

A helper shell script that executes all formatters is included under `tools/format.sh`.
