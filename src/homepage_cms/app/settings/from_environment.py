import environ

env = environ.Env(DEBUG=(bool, False))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env("DEBUG")

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env("SECRET_KEY")

DATABASES = {"default": env.db("DEFAULT_DATABASE")}

# Wagtail settings

WAGTAIL_SITE_NAME = env("WAGTAIL_SITE_NAME")

# Base URL to use when referring to full URLs within the Wagtail admin backend -
# e.g. in notification emails. Don't include '/admin' or a trailing slash
BASE_URL = env.str("BASE_URL")

# Preview settings

HEADLESS_PREVIEW_CLIENT_URLS = env.dict("HEADLESS_PREVIEW_CLIENT_URLS")

HEADLESS_PREVIEW_LIVE = env.bool("HEADLESS_PREVIEW_LIVE", True)
