from django.core.management.utils import get_random_secret_key

from .base import *

DEBUG = False

# SECURITY WARNING: generate a random secret key during build
SECRET_KEY = get_random_secret_key()

# Use a sql lite database
DATABASES = {}

# Set an empty site name during build process
WAGTAIL_SITE_NAME = ""

# Set the base url to 0.0.0.0 during build process
BASE_URL = "http://0.0.0.0:8000"

ALLOWED_HOSTS = []

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = "/static/"

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"
