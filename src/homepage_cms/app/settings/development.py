from .base import *
from .from_environment import *

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]

# SECURITY WARNING: in production allowed origins should be specified
CORS_ALLOW_ALL_ORIGINS = True

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "build/static")
STATIC_URL = "/static/"

MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
MEDIA_URL = "/media/"


try:
    from .local import *
except ImportError:
    pass
