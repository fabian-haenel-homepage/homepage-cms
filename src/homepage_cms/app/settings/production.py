from .base import *
from .from_environment import *
from urllib import parse

ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=[])

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = parse.urljoin(BASE_URL, "/static/")

MEDIA_ROOT = env.str("MEDIA_ROOT", "/var/media")
MEDIA_URL = env.str("MEDIA_URL")

CORS_ALLOWED_ORIGINS = env.list("CORS_ALLOWED_ORIGINS", default=[])
