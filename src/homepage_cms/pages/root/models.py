from wagtail.core.models import Page
from wagtail_headless_preview.models import HeadlessPreviewMixin


class RootPage(HeadlessPreviewMixin, Page):
    parent_page_types = [Page]
