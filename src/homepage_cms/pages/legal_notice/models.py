from django.db.models import CharField, EmailField, TextField
from django.forms import TextInput
from django_countries.fields import CountryField
from grapple.models import GraphQLStreamfield, GraphQLString
from wagtail.admin.edit_handlers import StreamFieldPanel, FieldPanel
from wagtail.core.blocks import CharBlock, TextBlock
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail_headless_preview.models import HeadlessPreviewMixin

from homepage_cms.lib.blocks import LinkBlock
from homepage_cms.pages.root.models import RootPage


class LegalNoticePage(HeadlessPreviewMixin, Page):
    full_name = TextField()
    street = TextField()
    house_number = TextField()
    postal_code = CharField(max_length=10)
    city = TextField()
    country = CountryField()
    phone_number = CharField(max_length=15, blank=True)
    email = EmailField()
    content = StreamField(
        [
            ("H2", CharBlock()),
            ("H3", CharBlock()),
            ("H4", CharBlock()),
            ("Annotation", TextBlock()),
            ("Paragraph", TextBlock()),
            ("Link", LinkBlock()),
        ]
    )

    content_panels = Page.content_panels + [
        FieldPanel("full_name", widget=TextInput),
        FieldPanel("street", widget=TextInput),
        FieldPanel("house_number", widget=TextInput),
        FieldPanel("postal_code"),
        FieldPanel("city", widget=TextInput),
        FieldPanel("country"),
        FieldPanel("phone_number"),
        FieldPanel("email"),
        StreamFieldPanel("content"),
    ]

    graphql_fields = [
        GraphQLString("full_name"),
        GraphQLString("street"),
        GraphQLString("house_number"),
        GraphQLString("postal_code"),
        GraphQLString("city"),
        GraphQLString("country"),
        GraphQLString("phone_number"),
        GraphQLString("email"),
        GraphQLStreamfield("content"),
    ]

    parent_page_types = [RootPage]
    subpage_types = []
