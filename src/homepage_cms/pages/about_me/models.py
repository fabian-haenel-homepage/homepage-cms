from django.db import models
from django.db.models import Model, ForeignKey, TextField
from django.forms import CheckboxSelectMultiple
from grapple.models import (
    GraphQLStreamfield,
    GraphQLCollection,
    GraphQLForeignKey,
    GraphQLImage,
    GraphQLString,
)
from modelcluster.fields import ParentalManyToManyField
from wagtail.admin.edit_handlers import StreamFieldPanel, FieldPanel
from wagtail.core.blocks import CharBlock, TextBlock
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail_headless_preview.models import HeadlessPreviewMixin

from homepage_cms.lib.blocks import LinkBlock
from homepage_cms.lib.models import Technology
from homepage_cms.pages.about_me.blocks import (
    ExperienceBlock,
    EducationBlock,
    LanguageBlock,
)
from homepage_cms.pages.root.models import RootPage


class AboutMePage(HeadlessPreviewMixin, Page):
    title_image = ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    description = StreamField(
        [
            ("SubTitle", CharBlock()),
            ("Annotation", TextBlock()),
            ("Paragraph", TextBlock()),
            ("Link", LinkBlock()),
            ("Image", ImageChooserBlock()),
        ]
    )
    experiences = StreamField([("experience", ExperienceBlock())], blank=True)
    technology_comment = TextField(blank=True)
    technologies = ParentalManyToManyField(Technology, blank=True)
    education = StreamField([("title", EducationBlock())], blank=True)
    languages = StreamField([("language", LanguageBlock())], blank=True)

    content_panels = Page.content_panels + [
        ImageChooserPanel("title_image"),
        StreamFieldPanel("description"),
        StreamFieldPanel("experiences"),
        StreamFieldPanel("education"),
        FieldPanel("technology_comment"),
        FieldPanel("technologies", widget=CheckboxSelectMultiple),
        StreamFieldPanel("languages"),
    ]
    graphql_fields = [
        GraphQLImage("title_image"),
        GraphQLStreamfield("description"),
        GraphQLStreamfield("experiences"),
        GraphQLString("technology_comment"),
        GraphQLCollection(
            GraphQLForeignKey,
            "technologies",
            "lib.Technology",
            is_paginated_queryset=False,
        ),
        GraphQLStreamfield("education"),
        GraphQLStreamfield("languages"),
    ]

    parent_page_types = [RootPage]
    subpage_types = []
