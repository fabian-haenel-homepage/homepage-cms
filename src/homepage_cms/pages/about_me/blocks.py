from django.db.models import IntegerChoices
from grapple.helpers import register_streamfield_block
from grapple.models import (
    GraphQLString,
    GraphQLInt,
    GraphQLImage,
    GraphQLCollection,
)
from wagtail.core.blocks import (
    StructBlock,
    CharBlock,
    DateBlock,
    ListBlock,
    ChoiceBlock,
)
from wagtail.images.blocks import ImageChooserBlock

from homepage_cms.lib.validators import ImageHeightToWidthRatioValidator


@register_streamfield_block
class ExperienceBlock(StructBlock):
    start_date = DateBlock()
    end_date = DateBlock(required=False)
    role = CharBlock()
    icon = ImageChooserBlock(validators=[ImageHeightToWidthRatioValidator(1, 1)])
    tags = ListBlock(CharBlock(), blank=True)
    company = CharBlock(required=False)

    graphql_fields = [
        GraphQLString("start_date"),
        GraphQLString("end_date"),
        GraphQLString("role"),
        GraphQLImage("icon"),
        GraphQLCollection(GraphQLString, "tags"),
        GraphQLString("company"),
    ]


@register_streamfield_block
class EducationBlock(StructBlock):
    start_date = DateBlock()
    end_date = DateBlock(required=False)
    title = CharBlock()
    graduation_topic = CharBlock()
    institute = CharBlock()
    institute_logo = ImageChooserBlock(
        validators=[ImageHeightToWidthRatioValidator(1, 1)]
    )

    graphql_fields = [
        GraphQLString("start_date"),
        GraphQLString("end_date"),
        GraphQLString("title"),
        GraphQLString("graduation_topic"),
        GraphQLString("institute"),
        GraphQLImage("institute_logo"),
    ]


class LanguageLevel(IntegerChoices):
    NONE = 0, "None"
    BASIC = 5, "Basic"
    ADVANCED = 10, "Advanced"
    FLUENT = 15, "Fluent"
    NATIVE = 20, "Native"


@register_streamfield_block
class LanguageBlock(StructBlock):
    language = CharBlock()
    level = ChoiceBlock(choices=LanguageLevel.choices)

    graphql_fields = [
        GraphQLString("language"),
        GraphQLInt("level"),
    ]
