import graphene
from grapple.types.pages import PageInterface
from grapple.types.structures import QuerySetList
from grapple.utils import resolve_queryset
from django.utils.translation import ugettext_lazy as _

from wagtail.core.models import Page as WagtailPage, Site
from grapple.types.sites import SiteObjectType as GrappleSiteObjectType
from django.contrib.contenttypes.models import ContentType


class SiteObjectType(GrappleSiteObjectType):
    pages = QuerySetList(
        graphene.NonNull(lambda: PageInterface),
        enable_search=True,
        required=True,
        content_type=graphene.Argument(
            graphene.String,
            description=_("Filter by content type. Uses the `app.Model` notation."),
        ),
        show_in_menus=graphene.Argument(
            graphene.Boolean,
            description=_("Filter by show in menu flag."),
        ),
    )

    def resolve_pages(self, info, **kwargs):
        pages = WagtailPage.objects.in_site(self).live().public().specific()

        content_type = kwargs.pop("content_type", None)
        if content_type:
            app_label, model = content_type.strip().lower().split(".")
            try:
                ctype = ContentType.objects.get(app_label=app_label, model=model)
            except ContentType.DoesNotExist:
                return (
                    WagtailPage.objects.none()
                )  # something not quite right here, bail out early
            else:
                pages = pages.filter(content_type=ctype)

        show_in_menus = kwargs.pop("show_in_menus", None)
        if show_in_menus is not None:
            pages = pages.filter(show_in_menus=show_in_menus)

        return resolve_queryset(pages, info, **kwargs)

    class Meta:
        model = Site


class SitesQuery:
    site = graphene.Field(SiteObjectType, hostname=graphene.String(), id=graphene.ID())
    sites = QuerySetList(
        graphene.NonNull(SiteObjectType),
        enable_search=True,
        required=True,
    )

    # Return all sites.
    def resolve_sites(self, info, **kwargs):
        return resolve_queryset(Site.objects.all(), info, **kwargs)

    # Return a site, identified by ID or hostname.
    def resolve_site(self, info, **kwargs):
        pk, hostname = kwargs.get("id"), kwargs.get("hostname")

        try:
            if pk:
                return Site.objects.get(pk=pk)
            elif hostname:
                return Site.objects.get(hostname=hostname)
        except BaseException:
            return None
