from grapple.types.sites import SitesQuery as GrappleSitesQuery
from wagtail.contrib.modeladmin.options import modeladmin_register
from wagtail.core import hooks

from homepage_cms.lib.types.sites import SitesQuery
from homepage_cms.lib.wagtail_admin import DeveloperGroup

modeladmin_register(DeveloperGroup)


@hooks.register("register_schema_query", order=100)
def register_custom_site_query(query_mixins: list):
    qualname = GrappleSitesQuery().__qualname__
    query_mixins[:] = [item for item in query_mixins if item.__qualname__ != qualname]
    query_mixins.append(SitesQuery)
