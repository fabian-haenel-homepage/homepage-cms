import os
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from django.utils.deconstruct import deconstructible
from wagtail.images.models import (
    Image,
)  # To get the image object from wagtail collections


@deconstructible
class ImageHeightToWidthRatioValidator:
    def __init__(
        self,
        min_ratio: float,
        max_ratio: float,
    ):
        self.min_ratio = min_ratio
        self.max_ratio = max_ratio

    def __call__(self, image):
        img = (
            image if isinstance(image, Image) else Image.objects.get(id=image)
        )  # Getting the image object from wagtail collections

        w, h = get_image_dimensions(img.file)

        ratio = w / h

        if ratio < self.min_ratio:
            raise ValidationError(
                f"{img.filename} the minimum ration of height to width must not be less"
                f" than {self.min_ratio}"
            )

        if ratio > self.max_ratio:
            raise ValidationError(
                f"{img.filename} the maximum ration of height to width must not be more"
                f" than {self.max_ratio}"
            )
