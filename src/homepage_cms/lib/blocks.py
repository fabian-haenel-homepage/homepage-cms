from grapple.helpers import register_streamfield_block
from grapple.models import GraphQLString
from wagtail.core.blocks import StructBlock, CharBlock, URLBlock


@register_streamfield_block
class LinkBlock(StructBlock):
    label = CharBlock()
    url = URLBlock()

    graphql_fields = [
        GraphQLString("label", source="resolve_label"),
        GraphQLString("url"),
    ]

    def resolve_label(self, values):
        return values.get("label")
