from django.db.models import Model, CharField, TextChoices, ForeignKey
from django.db import models
from grapple.models import GraphQLImage, GraphQLString
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from homepage_cms.lib.validators import ImageHeightToWidthRatioValidator


class TechnologyCategory(TextChoices):
    PROGRAMMING_LANGUAGE = "Programming Language", "Programming Language"
    FRAMEWORK = "Framework", "Framework"
    DATABASE = "Database", "Database"
    API = "API", "API"
    DEV_OPS = "DevOps", "DevOps"


class Technology(Model):
    name = CharField(max_length=50)
    category = CharField(max_length=20, choices=TechnologyCategory.choices)
    logo = ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        validators=[ImageHeightToWidthRatioValidator(1, 5)],
    )

    panels = [
        FieldPanel("name"),
        FieldPanel("category"),
        ImageChooserPanel("logo"),
    ]

    graphql_fields = [
        GraphQLString("name"),
        GraphQLString("category"),
        GraphQLImage("logo"),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Technology"
        verbose_name_plural = "Technologies"
