from wagtail.contrib.modeladmin.options import (
    ModelAdmin,
    ModelAdminGroup,
)

from homepage_cms.lib.models import Technology


class TechnologyAdmin(ModelAdmin):
    model = Technology
    menu_label = "Technologies"
    list_display = "name", "category"
    list_filter = ("category",)
    search_fields = ("name",)


class DeveloperGroup(ModelAdminGroup):
    menu_label = "Developer"
    menu_icon = "folder-open-inverse"
    menu_order = 200
    items = (TechnologyAdmin,)
