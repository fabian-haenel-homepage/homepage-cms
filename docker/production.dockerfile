FROM node:16-buster-slim AS tailwind-build

ARG NODE_ENV="production"

WORKDIR /app

COPY .yarn/releases ./.yarn/releases

COPY .yarnrc.yml package.json yarn.lock ./

RUN yarn install --immutable

COPY tailwind.config.js postcss.config.js ./
COPY src ./src

RUN npx tailwindcss -i src/tailwind.css -o /dist/tailwind.css

# Use an official Python runtime based on Debian 10 "buster" as a parent image.
FROM python:3.9-slim-buster

# Add user that will be used in the container.
RUN useradd wagtail

# Port used by this container to serve HTTP.
EXPOSE 8000

# Set environment variables.
# 1. Force Python stdout and stderr streams to be unbuffered.
# 2. Set PORT variable that is used by Gunicorn. This should match "EXPOSE"
#    command.
ENV PYTHONUNBUFFERED=1 \
    PORT=8000

# Install system packages required by Wagtail and Django.
RUN apt-get update --yes --quiet && apt-get install --yes --quiet --no-install-recommends \
    build-essential \
    libpq-dev \
    libmariadbclient-dev \
    libjpeg62-turbo-dev \
    zlib1g-dev \
    libwebp-dev \
 && rm -rf /var/lib/apt/lists/*

# Install the application server.
RUN pip install "gunicorn==20.0.4" "pipenv==2021.5.29"

# Setup docker entrypoint.
COPY docker/docker-entrypoint.sh /
COPY docker/docker-entrypoint.d /docker-entrypoint.d/

RUN chmod +x docker-entrypoint.sh \
        docker-entrypoint.d/*.sh \
        docker-entrypoint.d/*.py

ENTRYPOINT ["/docker-entrypoint.sh"]

# Use /app folder as a directory where the source code is stored.
WORKDIR /app

# Install the project requirements.
COPY Pipfile Pipfile.lock ./
RUN pipenv sync --system --bare \
    && pipenv --rm

# Set this directory to be owned by the "wagtail" user. This Wagtail project
# uses SQLite, the folder needs to be owned by the user that
# will be writing to the database file.
RUN chown wagtail:wagtail /app

# Copy the source code of the project into the container.
COPY --chown=wagtail:wagtail src .
# Copy and overwrite tailwind.css file from build stage
COPY --chown=wagtail:wagtail --from=tailwind-build /dist ./homepage_cms/app/static/css/

# Use user "wagtail" to run the build commands below and the server itself.
USER wagtail

# Use build settings.
ARG DJANGO_SETTINGS_MODULE=homepage_cms.app.settings.build

# Collect static files.
RUN python manage.py collectstatic --noinput --clear

# Configure default environment to use production settings.
ENV DJANGO_SETTINGS_MODULE=homepage_cms.app.settings.production

# Set default startup command.
CMD ["gunicorn", "homepage_cms.app.wsgi:application"]

# Tag application version.
ARG VERSION=$VERSION
ENV VERSION=$VERSION
