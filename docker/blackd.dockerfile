# Use an official Python runtime based on Debian 10 "buster" as a parent image.
FROM python:3.9-slim-buster

WORKDIR /black

EXPOSE 45484

COPY docker/get_black_version.py Pipfile.lock ./

RUN pip install $(python get_black_version.py)

CMD ["blackd", "--bind-host", "0.0.0.0", "--bind-port", "45484"]
