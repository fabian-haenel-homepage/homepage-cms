#!/usr/bin/env bash

set -e

if [[ $(stat -L -c "%u" "${MEDIA_ROOT:-/var/media}") != $(id -u) ]]; then
  echo >&3 "$0: Ownership of media folder is incorrectly set. The media folder must be owned by the user running wagtail."
  exit 1
fi
