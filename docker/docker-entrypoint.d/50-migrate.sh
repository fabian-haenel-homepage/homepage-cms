#!/usr/bin/env bash

set -e

if [ -z "${DJANGO_MIGRATE_ON_STARTUP:-}" ]; then
  echo >&3 "$0: Skipping running django migrations";
else
  echo >&3 "$0: Running django migrations";
  python manage.py migrate --noinput
fi
