#!/usr/bin/env python

import os

print(
    f"""
{'=' * 80}
=
= Homepage CMS {os.environ.get('VERSION', 'canary')}
=
{'=' * 80}
"""
)
