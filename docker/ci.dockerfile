FROM python:3.9-slim-buster

RUN apt-get update --yes --quiet \
    && apt-get install --yes --quiet --no-install-recommends \
        build-essential \
        libpq-dev \
        libmariadbclient-dev \
        libjpeg62-turbo-dev \
        zlib1g-dev \
        libwebp-dev \
    && rm -rf /var/lib/apt/lists/*

RUN pip install "pipenv==2021.5.29"

# Install the project requirements.
COPY Pipfile Pipfile.lock ./
RUN pipenv sync --system --dev --bare \
    && pipenv --rm
