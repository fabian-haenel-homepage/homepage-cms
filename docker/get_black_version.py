import json

with open("Pipfile.lock") as file:
    pipenv_installation = json.load(file)
    for install_target in {"develop", "default"}:
        if (installs := pipenv_installation.get(install_target)) and (
            black := installs.get("black")
        ):
            version = black["version"]
            extras = black.get("extras", ["d"])
            output = f"black[{','.join(extras)}]{version}"
            print(output)
            break
